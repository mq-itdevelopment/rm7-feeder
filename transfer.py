#!/usr/bin/env python3
import os
import logging
import datetime
import argparse
import glob
import cx_Oracle
import csv
import pysftp
import paramiko

from load_param import load_parameters

os.environ["NLS_LANG"] = ".AL32UTF8"


def ssh_execution(hostname, username, command):
    logging.info('ssh to {}'.format(hostname))
    hostname = hostname.rstrip("\n").strip()
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.load_host_keys(os.path.expanduser(os.path.join("~", ".ssh",
                                                       "known_hosts")))
    ssh.connect(hostname, username=username, look_for_keys=True)
    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(command, get_pty=True)
    for line in iter(ssh_stdout.readline, ""):
        logging.info(line)
    for line in iter(ssh_stderr.readline, ""):
        logging.error(line)
    returnCode = ssh_stdout.channel.recv_exit_status()
    print('ssh return code = ${}'.format(returnCode))
    if returnCode != 0:
        ssh.close()
        raise Exception('ssh execution exception')
    ssh.close()


def download_data(prop, table, file_prefix):
    dbUrl = prop['dbUrl']
    dbUser = prop['dbUser']
    dbPasswd = prop['dbPasswd']

    logging.info('downloading data from db')

    # Connect to database
    db = cx_Oracle.connect(dbUser, dbPasswd, dbUrl)
    if db.version:
        logging.info('db is connected.')
    cur = db.cursor()
    # dateFormat = '{:%d%m%Y}'.format(datetime.datetime.now())
    # csvFileName = file_prefix + '_' + dateFormat + '.csv'
    csvFileName = file_prefix + '.csv'
    csvFile = open(csvFileName, 'w')
    output = csv.writer(csvFile, dialect='excel')
    sql = '''select * from ''' + table

    cur.execute(sql)
    # add header
    cols = []
    for col in cur.description:
        cols.append(col[0])
    output.writerow(cols)
    # add rows
    for row in cur:
        output.writerow(row)

    csvFile.close()
    cur.close()
    db.close()

    logging.debug("download data from db completed")

    return csvFileName


def transfer_data(prop, fileName):
    ftpProxyServer = prop['ftpProxyServer']
    ftpProxyUser = prop['ftpProxyUser']
    ftpProxyDir = prop['ftpProxyDir']

    ftpServer = prop['ftpServer']
    ftpUser = prop['ftpUser']
    # ftpPasswd = prop['ftpPasswd']
    ftpDir = prop['ftpDir']

    logging.info('Transfering data to ftp proxy server')

    cnopts = pysftp.CnOpts()
    cnopts.hostkeys = None
    sftp = pysftp.Connection(ftpProxyServer, username=ftpProxyUser,
                             private_key='~/.ssh/id_rsa', cnopts=cnopts)
    sftp.cd(ftpDir)
    sftp.put(fileName, ftpProxyDir + '/' + fileName)
    logging.info("put file " + fileName + " to " + ftpProxyDir)

    logging.info('Transfering data to ELMO ftp server')

    sftpCommand = 'sftp ' + ftpUser + '@' + ftpServer + \
        ':' + ftpDir + """ <<< $'put """ + ftpProxyDir + \
        '/' + fileName + """'"""

    logging.debug(sftpCommand)
    ssh_execution(ftpProxyServer, ftpProxyUser, sftpCommand)

    purgeCommand = 'find ' + ftpProxyDir + \
        ' -type f -mtime +14 -exec rm -f {} \;'
    logging.debug(purgeCommand)
    ssh_execution(ftpProxyServer, ftpProxyUser, purgeCommand)

    logging.info('Transfer data to ELMO ftp server completed')


def main():
    try:
        # deal with input parameters
        parser = argparse.ArgumentParser()
        parser.add_argument('-config', '--config',
                            dest='config',
                            default="config.json",
                            help='Configuration file in JSON format')
        args = parser.parse_args()
        config_file = args.config

        # load paramters
        prop = load_parameters(config_file)
        log_dir = prop['logDir']

        if not os.path.exists(log_dir):
            os.makedirs(log_dir)

        logfile = os.path.join(
            log_dir, 'ResearchMaster-transfer{:-%Y-%m-%d-%H-%M-%S}.log'
            .format(datetime.datetime.now()))

        log_level = prop['logLevel']

        llevel = logging.INFO

        if log_level == 'DEBUG':
            llevel = logging.DEBUG
        elif log_level == 'INFO':
            llevel = logging.INFO
        elif log_level == 'ERROR':
            llevel = logging.ERROR
        elif log_level == 'WARNING':
            llevel = logging.WARNING

        logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s',
                            filename=logfile, level=llevel)
        files = 'datafiles'
        if not os.path.exists(files):
            os.makedirs(files)

        os.chdir(files)
        os.getcwd()
        filelist = glob.glob("*.csv")
        for f in filelist:
            os.remove(f)

        logging.debug('logging environment variables')

        for p in prop:
            logging.debug('{}={}'.format(p, prop[p]))

        # list of (view name, file prefix)
        feed_list = [('VW_HR_PERSONNEL_OUTPUT', 'HR_Personnel'),
                     ('VW_HR_PERSON_ORG_UNIT_OUTPUT', 'HR_PersonOrgUnit'),
                     ('VW_HR_PERSON_EMPLOYMENT_OUTPUT', 'HR_PersonEmployment'),
                     ('VW_HR_PER_PREV_COURSE_OUTPUT', 'HR_PersonPrevCourse'),
                     ('VW_STU_CORE_OUTPUT', 'STU_student'),
                     ('VW_STU_COURSE_OUTPUT', 'STU_Course'),
                     ('VW_STU_SUPERVISOR_OUTPUT', 'STU_Supervisor'),
                     ('VW_STU_LEAVE_OUTPUT', 'STU_Leave'),
                     ('VW_STU_STUDY_MODE_OUTPUT', 'STU_studymode'),
                     ('VW_COURSE_OUTPUT', 'Course_Ref_Data'),
                     ('VW_STU_ORG_UNIT_OUTPUT', 'STU_OrgUnit')]
        for t in feed_list:
            # Download
            logging.info('Download begins for ' + t[0])
            fileName = download_data(prop, t[0], t[1])
            logging.info('Download completed for ' + t[0])
            # transfer
            logging.info('Transfer begins for ' + fileName)
            transfer_data(prop, fileName)
            logging.info('Transfer completed for ' + fileName)

    except Exception as e:
        logging.error(e)
        logging.info("Job failed.")
        raise(e)


if __name__ == "__main__":
    main()
    exit()
