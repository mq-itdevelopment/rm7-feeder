#!/bin/bash
# Download data from Databank in csv file and transfer it to ELMO FTP server
CONFIGFILE=dev.json
#ulimit -s unlimited
# proxy server
#export https_proxy=http://proxy.solar.mq.edu.au:3128
#export http_proxy=http://proxy.solar.mq.edu.au:3128
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR
./transfer.py --config=$CONFIGFILE || exit 1
