import json


def load_parameters(config='config.json'):
    configjson = config
    if not configjson:
        configjson = 'config.json'
    prop = {}
    with open(configjson) as f:
        config_data = json.load(f)
        # log
        prop['logDir'] = config_data['logDir']
        prop['logLevel'] = config_data['logLevel']
        # ftp proxy variables
        prop['ftpProxyServer'] = config_data['ftpProxy']['server']
        prop['ftpProxyUser'] = config_data['ftpProxy']['user']
        prop['ftpProxyDir'] = config_data['ftpProxy']['dir']
        # ftp variables
        prop['ftpServer'] = config_data['ftp']['server']
        prop['ftpUser'] = config_data['ftp']['user']
        prop['ftpPasswd'] = config_data['ftp']['passwd']
        prop['ftpDir'] = config_data['ftp']['dir']

        # database variables
        prop['dbUser'] = config_data['db']['user']
        prop['dbPasswd'] = config_data['db']['passwd']
        prop['dbUrl'] = config_data['db']['url']

        return prop
